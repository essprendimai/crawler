# CRAWLER

## Introduction
    * ...
    
## Requirements
    * PHP: `>= 7.2`

## Installation
    * Download project
    * `php artisan key:generate`
    * `php artisan queue:table`
    * `php artisan queue:failed-table`
    * `php artisan migrate`
    * `php artisan module:migrate`
    * `php artisan db:seed`
    * `php artisan module:seed`
    * `npm install`
    * Optional - For more donwload size its important up post_max_size in php.ini file if cant upload files

## Test
* Run tests: `composer run test`

## Build assets
* npm install
* npm run build - production
* npm run dev  development
