<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Job
 */
class Job extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'jobs';

    /**
     * @var array
     */
    protected $fillable = [
        'queue',
        'payload',
        'attempts',
        'reserved_at',
        'available_at',
        'created_at',
    ];
}
