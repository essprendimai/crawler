<?php

namespace App\Enums;

/**
 * Class Enumerable
 */
abstract class Enumerable
{
    /** @var Enumerable[] */
    private static $instances = [];
    /** @var mixed */
    protected $id;
    /** @var string */
    protected $name;
    /** @var string */
    protected $description;

    /**
     * Enumerable constructor.
     *
     * @param $id
     * @param string $name
     * @param string $description
     */
    public function __construct($id, string $name, string $description = '')
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;

        // Register instance
        self::$instances[get_called_class()][$id] = $this;
    }

    /**
     * @param $id
     * @return Enumerable
     * @throws \Exception
     */
    public static function from($id): Enumerable
    {
        $enums = self::enum();
        if (!isset($enums[$id])) {
            throw new \Exception(strtr('Unable to find enumerable with id :id of type :type', [
                ':id' => $id,
                ':type' => get_called_class(),
            ]));
        }

        return $enums[$id];
    }

    /**
     * @return Enumerable[]
     */
    public static function enum(): array
    {
        $reflection = new \ReflectionClass(get_called_class());
        $finalMethods = $reflection->getMethods(\ReflectionMethod::IS_FINAL);

        $return = [];
        foreach ($finalMethods as $key => $method) {
            /** @var Enumerable $enum */
            $enum = $method->invoke(null);
            $return[$enum->id()] = $enum;
        }

        return $return;
    }

    /**
     * @return array
     */
    public static function options(): array
    {
        return array_map(function(Enumerable $enumerable) {
            return $enumerable->name();
        }, self::enum());
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public static function json(): string
    {
        return json_encode(array_map(function(Enumerable $enumerable) {
            return $enumerable->name();
        }, self::enum()));
    }

    /**
     * @param array $state
     * @return Enumerable|static
     */
    public static function __set_state(array $state): self
    {
        return self::make($state['id'], $state['name'], $state['description']);
    }

    /**
     * @param $id
     * @param string $name
     * @param string $description
     * @return $this|Enumerable
     */
    protected static function make($id, string $name, string $description = ''): Enumerable
    {
        $class = get_called_class();

        if (isset(self::$instances[$class][$id])) {
            return self::$instances[$class][$id];
        }

        $reflection = new \ReflectionClass($class);

        /** @var Enumerable $instance */
        $instance = $reflection->newInstance($id, $name, $description);
        $refConstructor = $reflection->getConstructor();
        $refConstructor->setAccessible(true);

        return self::$instances[$class][$id] = $instance;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->id();
    }

    /**
     * @return mixed
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @param string|null $name
     * @param string|null $selectedEnumId
     * @param string|null $id
     * @param string|null $class
     * @param bool $multiple
     * @param string|null $size
     * @param bool $translate
     * @param array $moreOptions
     *
     * @return string
     */
    public static function toHtmlSelectList(
        string $name = null,
        string $selectedEnumId = null,
        string $id = null,
        string $class = null,
        bool $multiple = false,
        string $size = null,
        bool $translate = false,
        array $moreOptions = []
    ): string {
        $selectList = '<select';
        if ($name) {
            $selectList .= ' name="' . $name . '" ';
        }

        if ($id) {
            $selectList .= ' id="' . $id . '" ';
        }

        if ($class) {
            $selectList .= ' class="' . $class . '" ';
        }

        if ($size) {
            $selectList .= ' size="' . $size . '" ';
        }

        if ($multiple) {
            $selectList .= ' multiple ';
        }

        foreach ($moreOptions as $moreOptionKey => $moreOptionValue) {
            $selectList .= ' ' . $moreOptionKey . '="' . $moreOptionValue . '" ';
        }

        $selectList .= '>';

        foreach (self::enum() as $enum) {
            $selectList .= '<option value="' . $enum->id . '" ' . ($enum->id === $selectedEnumId ? 'selected' : '') . '>' . ($translate ? __($enum->name) : $enum->name) . '</option>';
        }

        $selectList .= '</select>';

        return $selectList;
    }
}
