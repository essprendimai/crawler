<?php

namespace App\Enums;

/**
 * Class QueueEnum
 */
class QueueEnum extends Enumerable
{
    /**
     * @return QueueEnum
     */
    final public static function defaultQueue(): self
    {
        return self::make('default', 'Default', 'Default queue');
    }
}
