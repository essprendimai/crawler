<?php

namespace App\Repositiories;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use InvalidArgumentException;
use RuntimeException;

/**
 * Class Repository
 * @package Sanitex\Basic\Repositories
 */
abstract class Repository implements RepositoryContract
{
    const DEFAULT_PER_PAGE = 15;

    /**
     * Default attributes field used for whereIn and update methods
     */
    const DEFAULT_ATTRIBUTES_FIELD = 'id';

    /**
     * @return string
     */
    abstract public function model(): string;

    /**
     * @param array $columns
     * @return Collection
     * @throws RuntimeException
     */
    public function all(array $columns = ['*']): Collection
    {
        return $this->makeQuery()->get($columns);
    }

    /**
     * @param array $with
     * @return Builder
     * @throws RuntimeException
     */
    public function with(array $with = []): Builder
    {
        return $this->makeQuery()->with($with);
    }

    /**
     * @param array $with
     * @param array $columns
     *
     * @return Collection
     */
    public function allWith(array $with = [], array $columns = ['*']): Collection
    {
        return $this->makeQuery()->with($with)->get($columns);
    }

    /**
     * @param int $perPage
     * @param array $columns
     * @return LengthAwarePaginator
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function paginate(int $perPage = self::DEFAULT_PER_PAGE, array $columns = ['*']): LengthAwarePaginator
    {
        return $this->makeQuery()->paginate($perPage, $columns);
    }

    /**
     * @param string $filed
     * @param int $perPage
     * @param array $columns
     * @return LengthAwarePaginator
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function paginateDesc(int $perPage = self::DEFAULT_PER_PAGE, string $filed = 'created_at', array $columns = ['*']): LengthAwarePaginator
    {
        return $this->makeQuery()->orderBy($filed, 'desc')->paginate($perPage, $columns);
    }

    /**
     * @param array $distinct
     * @param int $perPage
     * @param string $filed
     * @param array $columns
     * @return LengthAwarePaginator
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function paginateDistinctDesc(array $distinct, int $perPage = self::DEFAULT_PER_PAGE, string $filed = 'created_at', array $columns = ['*']): LengthAwarePaginator
    {
        return $this->makeQuery()->groupBy($distinct)->orderBy($filed, 'desc')->paginate($perPage, $columns);
    }

    /**
     * @param array $data
     * @return Model
     * @throws RuntimeException
     */
    public function create(array $data = []): Model
    {
        return $this->makeModel()->create($data);
    }

    /**
     * @param array $data
     * @return bool
     * @throws RuntimeException
     */
    public function insert(array $data): bool
    {
        return $this->makeQuery()->insert($data);
    }

    /**
     * @param array $data
     * @param $attributeValue
     * @param string $attributeField
     * @return int
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function update(array $data, $attributeValue, string $attributeField = self::DEFAULT_ATTRIBUTES_FIELD): int
    {
        array_forget($data, [
            '_method',
            '_token',
        ]);

        return $this->makeQuery()->where($attributeField, $attributeValue)->update($data);
    }

    /**
     * @param mixed $attributeValues
     * @param string $attributeField
     * @return Collection
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function findWhereIn(
        $attributeValues,
        string $attributeField = self::DEFAULT_ATTRIBUTES_FIELD
    ) {
        return $this->makeQuery()->whereIn($attributeField, $attributeValues)->get();
    }

    /**
     * @param array $data
     * @param $attributeValues
     * @param string $attributeField
     * @return int
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function updateWhereIn(
        array $data,
        $attributeValues,
        string $attributeField = self::DEFAULT_ATTRIBUTES_FIELD
    ): int {
        return $this->makeQuery()->whereIn($attributeField, $attributeValues)->update($data);
    }

    /**
     * @param array $data
     * @param array $attributeValues
     * @return int
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function updateWhere(
        array $data,
        array $attributeValues
    ): int {
        return $this->makeQuery()->where($attributeValues)->update($data);
    }

    /**
     * @param array $attributes
     * @param array $values
     * @return Model
     * @throws RuntimeException
     */
    public function updateOrCreate(array $attributes, array $values = []): Model
    {
        return $this->makeQuery()->updateOrCreate($attributes, $values);
    }

    /**
     * @param array $attributes
     * @param array $attributeValues
     * @param array $values
     * @return Model
     */
    public function updateOrCreateWhere(array $attributes, array $attributeValues, array $values = []): Model
    {
        return $this->makeQuery()->where($attributeValues)->updateOrCreate($attributes, $values);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function delete(int $id)
    {
        return $this->makeQuery()->where('id', $id)->delete();
    }

    /**
     * @param array $criteria
     * @return mixed
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function deleteWhere(array $criteria = [])
    {
        return $this->makeQuery()->where($criteria)->delete();
    }

    /**
     * @param int|string $id
     * @param array $columns
     * @return Builder|Builder[]|Collection|Model|null
     * @throws RuntimeException
     */
    public function find($id, array $columns = ['*'])
    {
        return $this->makeQuery()->find($id, $columns);
    }

    /**
     * @param int|string $id
     * @param array $columns
     * @return Builder|Builder[]|Collection|Model|null
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function findAndLock($id, array $columns = ['*'])
    {
        return $this->makeQuery()->lockForUpdate()->find($id, $columns);
    }

    /**
     * @param array $criteria
     * @param array $columns
     * @return Builder|Model|mixed
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function findOneBy(array $criteria = [], array $columns = ['*'])
    {
        return $this->makeQuery()->where($criteria)->first($columns);
    }

    /**
     * @param array $criteria
     * @param array $columns
     * @return Builder|Model|mixed
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function findOneByAndLock(array $criteria = [], array $columns = ['*'])
    {
        return $this->makeQuery()->where($criteria)->lockForUpdate()->first($columns);
    }

    /**
     * @param int|string $id
     * @param array $columns
     * @return Builder|Builder[]|Collection|Model|null
     * @throws RuntimeException
     */
    public function findOrFail($id, array $columns = ['*'])
    {
        return $this->makeQuery()->findOrFail($id, $columns);
    }

    /**
     * @param int|string $id
     * @param array $columns
     * @return Builder|Builder[]|Collection|Model|null
     * @throws RuntimeException
     */
    public function findAndLockOrFail($id, array $columns = ['*'])
    {
        return $this->makeQuery()->lockForUpdate()->findOrFail($id, $columns);
    }

    /**
     * @param array $criteria
     * @param array $columns
     * @return Collection
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function findAllBy(array $criteria = [], array $columns = ['*']): Collection
    {
        return $this->makeQuery()->select($columns)->where($criteria)->get();
    }

    /**
     * @param array $data
     * @return int
     * @throws RuntimeException
     */
    public function insertGetId(array $data): int
    {
        return $this->makeQuery()->insertGetId($data);
    }

    /**
     * @return int
     * @throws RuntimeException
     */
    public function count(): int
    {
        return $this->makeQuery()->count();
    }

    /**
     * @param array $where
     * @param array $incrementColumns
     * @param array $decrementColumns
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function whereIncrementAndDecrement(
        array $where,
        array $incrementColumns = [],
        array $decrementColumns = []
    ) {
        $update = [];
        foreach ($incrementColumns as $column => $increment) {
            $update[$column] = DB::raw('`' . $column . '` + ' . $increment);
        }

        foreach ($decrementColumns as $column => $increment) {
            $update[$column] = DB::raw('`' . $column . '` - ' . $increment);
        }

        $this->makeQuery()->where($where)->update($update);
    }

    /**
     * @return Model
     * @throws RuntimeException
     */
    final protected function makeModel(): Model
    {
        $model = app($this->model());

        if (!$model instanceof Model) {
            throw new RuntimeException('Class ' . $this->model() . ' must be an instance of Illuminate\\Database\\Eloquent\\Model');
        }

        return $model;
    }

    /**
     * @return Builder
     * @throws RuntimeException
     */
    final public function makeQuery(): Builder
    {
        return $this->makeModel()->newQuery();
    }
}
