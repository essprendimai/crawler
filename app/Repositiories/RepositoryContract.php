<?php

namespace App\Repositiories;

use Illuminate\Database\Eloquent\Builder;
use RuntimeException;

/**
 * Interface RepositoryContract
 */
interface RepositoryContract
{
    /**
     * @return string
     */
    public function model(): string;

    /**
     * @return Builder
     * @throws RuntimeException
     */
    public function makeQuery(): Builder;
}
