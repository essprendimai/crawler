<?php

namespace App\Repositiories;

use App\Entities\Job;
use App\Enums\Enumerable;
use Illuminate\Support\Collection;
use InvalidArgumentException;
use RuntimeException;

/**
 * Class JobRepository
 */
class JobRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Job::class;
    }

    /**
     * @param Enumerable $queue
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function deleteByQueue(Enumerable $queue)
    {
        $this->deleteWhere([
            'queue' => $queue->id(),
        ]);
    }

    /**
     * @param Enumerable $queue
     * @return Collection
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function getAllByQueue(Enumerable $queue): Collection
    {
        return $this->findAllBy([
            'queue' => $queue->id(),
        ]);
    }
}
