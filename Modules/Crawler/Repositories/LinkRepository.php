<?php

namespace Modules\Crawler\Repositories;

use App\Repositiories\Repository;
use Modules\Crawler\Entities\Link;

/**
 * Class LinkRepository
 */
class LinkRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Link::class;
    }
}