<?php

namespace Modules\Crawler\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Modules\Crawler\Entities\LinkCrawlerData;

/**
 * Class CrawlerDataEmail
 */
class CrawlerDataEmail extends Mailable implements ShouldQueue
{
    use Queueable;

    /**
     * @var LinkCrawlerData
     */
    public $linkCrawlerData;

    /**
     * DepsTimeTrackingReminderMail constructor.
     *
     * @param LinkCrawlerData $linkCrawlerData
     */
    public function __construct(LinkCrawlerData $linkCrawlerData)
    {
        $this->linkCrawlerData = $linkCrawlerData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): Mailable
    {
        return $this->subject(__('crawler::global.mail.subject'))
            ->markdown('crawler::mail.crawler_data');
    }
}