<?php

namespace Modules\Crawler\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\View\View;
use Modules\Crawler\Http\Requests\CrawlerFormRequest;
use Modules\Crawler\Http\Requests\ScrapViaMailRequest;
use Modules\Crawler\Repositories\LinkRepository;
use Modules\Crawler\Services\CrawlerMailService;
use Modules\Crawler\Services\CrawlerService;
use Throwable;

/**
 * Class CrawlerController
 */
class CrawlerController extends Controller
{
    /**
     * @var LinkRepository
     */
    private $linkRepository;

    /**
     * @var CrawlerService
     */
    private $crawlerService;

    /**
     * CrawlerController constructor.
     * @param LinkRepository $linkRepository
     * @param CrawlerService $crawlerService
     */
    public function __construct(LinkRepository $linkRepository, CrawlerService $crawlerService)
    {
        $this->linkRepository = $linkRepository;
        $this->crawlerService = $crawlerService;
    }

    /**
     * @return View
     */
    public function index(): View
    {
        $links = $this->linkRepository->all();

        return view('crawler::index', compact('links'));
    }

    /**
     * @param CrawlerFormRequest $request
     *
     * @return RedirectResponse
     */
    public function store(CrawlerFormRequest $request): RedirectResponse
    {
        try {
            $this->linkRepository->create([
                'search_link' => $request->getSearchUrl(),
                'link' => $request->getLinkUrl(),
            ]);
        } catch (Throwable $throwable) {
            logger()->error($throwable->getMessage(), $throwable->getTrace());

            return redirect()
                ->back()
                ->with('danger', __('crawler::global.failed_create'));
        }

        return redirect()
            ->back()
            ->with('success', __('crawler::global.success_create'));
    }

    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        try {
            $this->linkRepository->delete($id);
        } catch (Throwable $throwable) {
            logger()->error($throwable->getMessage(), $throwable->getTrace());

            return redirect()
                ->back()
                ->with('danger', __('crawler::global.failed_delete'));
        }

        return redirect()
            ->back()
            ->with('success', __('crawler::global.success_delete'));
    }

    /**
     * @param Request $request
     * @return View
     */
    public function scrap(Request $request): View
    {
        $data = $this->crawlerService->scrap();

        return view('crawler::scrap', compact('data'));
    }

    /**
     * @param ScrapViaMailRequest $request
     * @param CrawlerMailService $mailService
     * @return RedirectResponse
     */
    public function scrapViaMail(ScrapViaMailRequest $request, CrawlerMailService $mailService): RedirectResponse
    {
        try {
            $data = $this->crawlerService->scrap();
            $mailService->sendDataMail($data, $request->getEmail());
        } catch (Throwable $throwable) {
            logger()->error($throwable->getMessage(), $throwable->getTrace());

            return redirect()
                ->back()
                ->with('danger', __('crawler::global.failed_sent_mail'));
        }

        return redirect()
            ->back()
            ->with('success', __('crawler::global.success_sent_mail'));
    }
}
