<?php

namespace Modules\Crawler\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CrawlerFormRequest
 */
class CrawlerFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'search_url' => 'required|string|url',
            'link_url' => 'required|string|url',
        ];
    }

    /**
     * @return string
     */
    public function getSearchUrl(): string
    {
        return $this->input('search_url');
    }

    /**
     * @return string
     */
    public function getLinkUrl(): string
    {
        return $this->input('link_url');
    }
}