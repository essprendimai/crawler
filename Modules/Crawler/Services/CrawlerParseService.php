<?php

namespace Modules\Crawler\Services;

use Modules\Crawler\Entities\Link;

/**
 * Class CrawlerParseService
 */
class CrawlerParseService
{
    const REL_NOFOLLOW = 'nofollow';

    /**
     * @param Link $link
     * @param \DOMNodeList $links
     * @return array
     */
    public function parse(Link $link, \DOMNodeList $links): array
    {
        $found = false;
        $foundNoFollow = false;
        foreach ($links as $element) {
            $href = $element->getAttribute('href');
            if ($link->search_link === $href) {
                $found = true;

                $rel = $element->getAttribute('rel');
                if ($rel === self::REL_NOFOLLOW) {
                    $foundNoFollow = true;
                }
            }
        }

        return [
            'found' => $found,
            'found_nofollow' => $foundNoFollow,
        ];
    }
}