<?php

namespace Modules\Crawler\Services;

use Modules\Crawler\Entities\LinkCrawlerData;
use Modules\Crawler\Repositories\LinkRepository;

/**
 * Class CrawlerService
 */
class CrawlerService
{
    /**
     * @var CrawlerFetchService
     */
    private $fetchService;

    /**
     * @var CrawlerParseService
     */
    private $parseService;

    /**
     * @var LinkRepository
     */
    private $linkRepository;

    /**
     * CrawlerService constructor.
     * @param LinkRepository $linkRepository
     * @param CrawlerFetchService $fetchService
     * @param CrawlerParseService $parseService
     */
    public function __construct(
        LinkRepository $linkRepository,
        CrawlerFetchService $fetchService,
        CrawlerParseService $parseService
    ) {
        $this->fetchService = $fetchService;
        $this->parseService = $parseService;
        $this->linkRepository = $linkRepository;
    }

    /**
     * @return LinkCrawlerData
     */
    public function scrap(): LinkCrawlerData
    {
        $linkCrawlerData = new LinkCrawlerData();

        $links = $this->linkRepository->all();
        foreach ($links as $link) {
            $fetchData = $this->fetchService->fetch($link);
            $data = $this->parseService->parse($link, $fetchData);

            if ($data['found'] || $data['found_nofollow']) {
                if ($data['found']) {
                    $linkCrawlerData->increaseCount();
                    $linkCrawlerData->putLinksFound($link->link);
                }

                if ($data['found_nofollow']) {
                    $linkCrawlerData->putLinksFoundWithNoFollow($link->link);
                }
            } else {
                $linkCrawlerData->putLinksNotFound($link->link);
            }
        }

        return $linkCrawlerData;
    }
}