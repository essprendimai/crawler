<?php

namespace Modules\Crawler\Services;

use App\Enums\QueueEnum;
use Illuminate\Support\Facades\Mail;
use Modules\Crawler\Emails\CrawlerDataEmail;
use Modules\Crawler\Entities\LinkCrawlerData;

/**
 * Class CrawlerMailService
 */
class CrawlerMailService
{
    /**
     * @param LinkCrawlerData $linkCrawlerData
     * @param string $email
     */
    public function sendDataMail(LinkCrawlerData $linkCrawlerData, string $email): void
    {
        Mail::to($email)->send((new CrawlerDataEmail($linkCrawlerData))->onQueue(QueueEnum::defaultQueue()->id()));
    }
}