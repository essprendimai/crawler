<?php

namespace Modules\Crawler\Services;

use DOMDocument;
use Modules\Crawler\Entities\Link;

/**
 * Class CrawlerFetchService
 */
class CrawlerFetchService
{
    /**
     * @param Link $link
     * @return \DOMNodeList
     */
    public function fetch(Link $link): \DOMNodeList
    {
        $dom = new DOMDocument('1.0');
        @$dom->loadHTMLFile($link->link);

        return $dom->getElementsByTagName('a');
    }
}