<?php

namespace Modules\Crawler\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CrawlerDatabaseSeeder
 */
class CrawlerDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

         $this->call(LinkSeeder::class);
    }
}
