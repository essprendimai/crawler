<?php

namespace Modules\Crawler\Database\Seeders;

use Illuminate\Database\Seeder;
use LogicException;
use Modules\Crawler\Repositories\LinkRepository;

/**
 * Class LinkSeeder
 */
class LinkSeeder extends Seeder
{
    const SEARCH_URL = 'http://www.hostinger.com';

    /**
     * @throws LogicException
     */
    public function run(): void
    {
        /** @var LinkRepository $linkRepository */
        $linkRepository = app(LinkRepository::class);
        $linkRepository->insert([
            [
                'search_link' => self::SEARCH_URL,
                'link' => 'https://rekvizitai.vz.lt/imone/hostinger_uab/',
            ],
            [
                'search_link' => self::SEARCH_URL,
                'link' => 'https://edition.cnn.com/2018/12/18/asia/xi-jinping-china-speech-reform-intl/index.html',
            ],
        ]);
    }
}
