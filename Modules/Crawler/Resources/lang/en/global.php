<?php

return [
    'title' => 'Crawler',
    'search_url' => 'Search url',
    'link_url' => 'Link url',
    'create_crawler_search' => 'Create crawler search',

    'actions' => 'Actions',
    'delete' => 'Delete',
    'empty' => 'Empty',
    'scrap' => 'Scrap',
    'scrap_via_mail' => 'Scrap via mail',

    'success_create' => 'Successfully created',
    'success_update' => 'Successfully updated',
    'success_delete' => 'Successfully deleted',
    'success_sent_mail' => 'Successfully sent mail',

    'failed_create' => 'Fail create',
    'failed_update' => 'Fail update',
    'failed_delete' => 'Fail delete',
    'failed_sent_mail' => 'Fail sent mail',

    'count' => 'Count',
    'found_links_count' => 'Found links count:',
    'found_links_with_nofollow_count' => 'Found links with nofollow count:',
    'not_found_links_count' => 'Not found links count:',
    'found_links_with_nofollow' => 'Found links with nofollow',
    'not_found_links' => 'Not found links',

    'mail' => [
        'subject' => 'Crawler data',
    ],
];