@component('mail::message')
<h3>{{ __('crawler::global.count') }}</h3>

<p>{{ __('crawler::global.found_links_count') }} {{ $linkCrawlerData->getCount() }}</p>
<p>{{ __('crawler::global.found_links_with_nofollow_count') }} {{ count($linkCrawlerData->getLinksFoundWithNoFollow()) }}</p>
<p>{{ __('crawler::global.not_found_links_count') }} {{ count($linkCrawlerData->getLinksNotFound()) }}</p>

<h3>{{ __('crawler::global.found_links_with_nofollow') }}</h3>
@foreach ($linkCrawlerData->getLinksFoundWithNoFollow() as $key => $link)
<a href="{{ $link }}" target="_blank">{{ $key }}. {{ $link }}</a>
@endforeach

<h3>{{ __('crawler::global.not_found_links') }}</h3>
@foreach ($linkCrawlerData->getLinksNotFound() as $key => $link)
<a href="{{ $link }}" target="_blank">{{ $key }}. {{ $link }}</a>
@endforeach
@endcomponent
