@extends('crawler::layouts.app')

@section('title')
    {{ __('crawler::global.scrap') }}
@endsection

@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content w-30 m-auto">
            <div class="m-4">
                <h3>{{ __('crawler::global.count') }}</h3>

                <p>{{ __('crawler::global.found_links_count') }} {{ $data->getCount() }}</p>
                <p>{{ __('crawler::global.found_links_with_nofollow_count') }} {{ count($data->getLinksFoundWithNoFollow()) }}</p>
                <p>{{ __('crawler::global.not_found_links_count') }} {{ count($data->getLinksNotFound()) }}</p>

                <h3>{{ __('crawler::global.found_links_with_nofollow') }}</h3>
                @foreach ($data->getLinksFoundWithNoFollow() as $key => $link)
                    <a href="{{ $link }}" target="_blank">{{ $key }}. {{ $link }}</a>
                @endforeach

                <h3>{{ __('crawler::global.not_found_links') }}</h3>
                @foreach ($data->getLinksNotFound() as $key => $link)
                    <a href="{{ $link }}" target="_blank">{{ $key }}. {{ $link }}</a>
                @endforeach
            </div>
        </div>
    </div>
@stop