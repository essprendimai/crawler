<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>@yield('title')</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="/css/app.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    @stack('styles')

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body id="body" class="sidebar-mini skin-blue-light">
<div class="wrapper">
    @if ((isset($messages) && $messages) || !isset($messages))
        @include('crawler::layouts.messages')
    @endif

    @yield('content')
</div>

<script src="/js/basic-view/adminlte.js"></script>

@stack('pre.scripts')
@stack('scripts')
@stack('post.scripts')
</body>
</html>
