@if (
        (isset($errors) && count($errors)) ||
        isset($success) ||
        session('success') ||
        isset($danger) ||
        session('danger') ||
        isset($info) ||
        session('info') ||
        isset($warning) ||
        session('warning')
)
<div class="@if(isset($headerDark) && !$headerDark) alert-header-absolute @endif">
    <div class="row">
        <div class="col-md-12 mb-5 @if (isset($padding) && !$padding) p-0 @endif">
            @if (isset($errors) && count($errors))
                <div class="alert alert-danger">
                    <ul class="mb-0">
                        @foreach ($errors->all() as $error)
                            <li class="list-unstyled mb-0">{{ $error }}</li>
                        @endforeach
                    </ul>
                    <div class="float-right">
                        <i class="fas fa-times-circle" style="color: red; cursor: pointer;" data-dismiss="alert"></i>
                    </div>
                </div>
            @endif

            @if(isset($success))
                <div class="alert alert-success" role="alert">
                    {{ $success }}
                    <div class="float-right">
                        <i class="fas fa-times-circle" style="color: red; cursor: pointer;" data-dismiss="alert"></i>
                    </div>
                </div>
            @endif

            @if(session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                    <div class="float-right">
                        <i class="fas fa-times-circle" style="color: red; cursor: pointer;" data-dismiss="alert"></i>
                    </div>
                </div>
            @endif

            @if(isset($danger))
                <div class="alert alert-danger" role="alert">
                    {{ $danger }}
                    <div class="float-right">
                        <i class="fas fa-times-circle" style="color: red; cursor: pointer;" data-dismiss="alert"></i>
                    </div>
                </div>
            @endif

            @if(session('danger'))
                <div class="alert alert-danger" role="alert">
                    {{ session('danger') }}
                    <div class="float-right">
                        <i class="fas fa-times-circle" style="color: red; cursor: pointer;" data-dismiss="alert"></i>
                    </div>
                </div>
            @endif

            @if(isset($info))
                <div class="alert alert-info" role="alert">
                    {{ $info }}
                    <div class="float-right">
                        <i class="fas fa-times-circle" style="color: red; cursor: pointer;" data-dismiss="alert"></i>
                    </div>
                </div>
            @endif

            @if(session('info'))
                <div class="alert alert-info" role="alert">
                    {{ session('info') }}
                    <div class="float-right">
                        <i class="fas fa-times-circle" style="color: red; cursor: pointer;" data-dismiss="alert"></i>
                    </div>
                </div>
            @endif

            @if(isset($warning))
                <div class="alert alert-warning" role="alert">
                    {{ $warning }}
                    <div class="float-right">
                        <i class="fas fa-times-circle" style="color: red; cursor: pointer;" data-dismiss="alert"></i>
                    </div>
                </div>
            @endif

            @if(session('warning'))
                <div class="alert alert-warning" role="alert">
                    {{ session('warning') }}
                    <div class="float-right">
                        <i class="fas fa-times-circle" style="color: red; cursor: pointer;" data-dismiss="alert"></i>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
@endif

