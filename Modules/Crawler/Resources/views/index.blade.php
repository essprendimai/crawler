@extends('crawler::layouts.app')

@section('title')
    {{ __('crawler::global.title') }}
@endsection

@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content w-30 m-auto">
            <div class="m-4">
                <form action="{{ route('crawler.search.store') }}" method="POST">
                    @csrf

                    <label for="search-url">{{ __('crawler::global.search_url') }}</label>
                    <input id="search-url" name="search_url" type="text" class="form-control" value="{{ config('crawler.default.search_url') }}" width="100%"/>

                    <br/>

                    <label for="link-url">{{ __('crawler::global.link_url') }}</label>
                    <input id="link-url" name="link_url" type="text" class="form-control" value="" width="100%"/>

                    <br/>

                    <button class="float-right btn btn-success">{{ __('crawler::global.create_crawler_search') }}</button>
                </form>

                <br/>
                <br/>

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>{{ __('crawler::global.search_url') }}</th>
                        <th>{{ __('crawler::global.link_url') }}</th>
                        <th>{{ __('crawler::global.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if ($links->count())
                            @foreach ($links as $link)
                                <tr>
                                    <td>{{ $link->search_link }}</td>
                                    <td>{{ $link->link }}</td>
                                    <td>
                                        <form action="{{ route('crawler.search.destroy', $link->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')

                                            <button class="btn btn-danger">{{ __('crawler::global.delete') }}</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach

                            <tr>
                                <td colspan="3">
                                    <a href="{{ route('crawler.scrap') }}" class="btn btn-info" target="_blank">{{ __('crawler::global.scrap') }}</a>

                                    <br/>

                                    <form action="{{ route('crawler.scrap_via_mail') }}" method="POST">
                                        @csrf

                                        <div class="form-group d-inline">
                                            <input id="email" name="email" type="text" class="form-control d-inline"  style="width: 50%" value="{{ config('crawler.default.email') }}" width="50%"/>
                                            <button class="btn btn-danger d-inline">{{ __('crawler::global.scrap_via_mail') }}</button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @else
                           <tr>
                               <td colspan="3">{{ __('crawler::global.empty') }}</td>
                           </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
