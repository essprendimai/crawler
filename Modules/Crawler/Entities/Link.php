<?php

namespace Modules\Crawler\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Link
 */
class Link extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var array
     */
    protected $fillable = [
        'search_link',
        'link',
    ];
}