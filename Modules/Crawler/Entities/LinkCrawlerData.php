<?php

namespace Modules\Crawler\Entities;

/**
 * Class LinkCrawlerData
 */
class LinkCrawlerData
{
    /**
     * @var int
     */
    private $count = 0;

    /**
     * @var array
     */
    private $linksFound = [];

    /**
     * @var array
     */
    private $linksFoundWithNoFollow = [];

    /**
     * @var array
     */
    private $linksNotFound = [];

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return LinkCrawlerData
     */
    public function increaseCount(): LinkCrawlerData
    {
        $this->count++;

        return $this;
    }

    /**
     * @return array
     */
    public function getLinksFound(): array
    {
        return $this->linksFound;
    }

    /**
     * @param string $linksFound
     * @return LinkCrawlerData
     */
    public function putLinksFound(string $linksFound): LinkCrawlerData
    {
        $this->linksFound[] = $linksFound;

        return $this;
    }

    /**
     * @return array
     */
    public function getLinksFoundWithNoFollow(): array
    {
        return $this->linksFoundWithNoFollow;
    }

    /**
     * @param string $linksFoundWithNoFollow
     * @return LinkCrawlerData
     */
    public function putLinksFoundWithNoFollow(string $linksFoundWithNoFollow): LinkCrawlerData
    {
        $this->linksFoundWithNoFollow[] = $linksFoundWithNoFollow;

        return $this;
    }

    /**
     * @return array
     */
    public function getLinksNotFound(): array
    {
        return $this->linksNotFound;
    }

    /**
     * @param string $linksNotFound
     * @return LinkCrawlerData
     */
    public function putLinksNotFound(string $linksNotFound): LinkCrawlerData
    {
        $this->linksNotFound[] = $linksNotFound;

        return $this;
    }
}