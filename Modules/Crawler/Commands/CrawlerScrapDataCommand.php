<?php

namespace Modules\Crawler\Commands;

use Illuminate\Console\Command;
use Modules\Crawler\Services\CrawlerService;

/**
 * Class CrawlerScrapDataCommand
 */
class CrawlerScrapDataCommand extends Command
{
    const COMMAND = 'crawler:scrap';
    const NAME = 'Scraps data';

    /**
     * @var string
     */
    protected $signature = self::COMMAND;

    /**
     * @var string
     */
    protected $description = 'Scraps data job';

    /**
     * @param CrawlerService $crawlerService
     */
    public function handle(CrawlerService $crawlerService): void
    {
        $this->info('--- Starting scrap ---');
        $this->info('- Start date: ' . date('Y-m-d H:i:s'));

        $data = $crawlerService->scrap();

        $this->info('--- Finished. Results:');
        $this->info(__('crawler::global.count') . ' ' . $data->getCount());
        $this->info(__('crawler::global.found_links_with_nofollow_count') . ' ' . count($data->getLinksFoundWithNoFollow()));
        $this->info(__('crawler::global.not_found_links_count') . ' ' . count($data->getLinksNotFound()));

        $this->info(__('crawler::global.found_links_with_nofollow') . ' ' . count($data->getLinksNotFound()));
        foreach ($data->getLinksFoundWithNoFollow() as $key => $link) {
            $this->info($key . '. ' . $link);
        }

        $this->info(__('crawler::global.not_found_links') . ' ' . count($data->getLinksNotFound()));
        foreach ($data->getLinksNotFound() as $key => $link) {
            $this->info($key . '. ' . $link);
        }

        $this->info('- End date: ' . date('Y-m-d H:i:s'));
        $this->info('--- End scrap ---');
    }

    /**
     * @return string
     */
    public static function getLogFilePath(): string
    {
        return storage_path('logs/schedule/' . self::COMMAND . '.log');
    }

    /**
     * @return array
     */
    public static function getLogMailTo(): array
    {
        return config('mail.send_mail_to');
    }

}