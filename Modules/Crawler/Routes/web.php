<?php

Route::get('/', 'CrawlerController@index');

Route::prefix('crawler')->as('crawler.')->group(function() {
    Route::resource('/search', 'CrawlerController', [
        'except' => [
            'show', 'create', 'update',
        ],
    ]);
    Route::get('/search/scrap', 'CrawlerController@scrap')->name('scrap');
    Route::post('/search/scrap-via-mail', 'CrawlerController@scrapViaMail')->name('scrap_via_mail');
});
