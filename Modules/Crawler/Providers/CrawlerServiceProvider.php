<?php

namespace Modules\Crawler\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\Crawler\Repositories\LinkRepository;
use Modules\Crawler\Services\CrawlerFetchService;
use Modules\Crawler\Services\CrawlerMailService;
use Modules\Crawler\Services\CrawlerParseService;
use Modules\Crawler\Services\CrawlerService;

class CrawlerServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->app->register(ScheduleServiceProvider::class);

        $this->registerRepositories();
        $this->registerServices();
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('crawler.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'crawler'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/crawler');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/crawler';
        }, \Config::get('view.paths')), [$sourcePath]), 'crawler');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/crawler');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'crawler');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'crawler');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function registerRepositories(): void
    {
        $this->app->singleton(LinkRepository::class);
    }

    private function registerServices(): void
    {
        $this->app->singleton(CrawlerService::class);
        $this->app->singleton(CrawlerMailService::class);
        $this->app->singleton(CrawlerFetchService::class);
        $this->app->singleton(CrawlerParseService::class);
    }
}
