<?php

namespace Modules\Crawler\Providers;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\ServiceProvider;
use Modules\Crawler\Commands\CrawlerScrapDataCommand;

/**
 * Class ScheduleServiceProvider
 */
class ScheduleServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->app->booted(function (): void {
            /** @var Schedule $schedule */
            $schedule = app(Schedule::class);

            $schedule->command(CrawlerScrapDataCommand::COMMAND)
                ->name(CrawlerScrapDataCommand::NAME)
                ->dailyAt('00:00')
                ->sendOutputTo(CrawlerScrapDataCommand::getLogFilePath())
                ->emailOutputTo(CrawlerScrapDataCommand::getLogMailTo());
        });
    }

    public function register(): void
    {
        $this->registerCommands();
    }

    public function registerCommands(): void
    {
        $this->commands([
            CrawlerScrapDataCommand::class,
        ]);
    }
}